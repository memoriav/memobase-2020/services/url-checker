module gitlab.switch.ch/memoriav/memobase-2020/services/url-checker

go 1.23.2

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/goph/emperror v0.17.2
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server v0.0.0-20210219103459-bb3035f312fc
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793
)

require (
	github.com/bluele/gcache v0.0.2 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
