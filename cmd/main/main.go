package main

import (
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"flag"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/memostream"
	"gitlab.switch.ch/memoriav/memobase-2020/services/url-checker/pkg/memocrawler"
	sshtunnel "gitlab.switch.ch/memoriav/memobase-2020/services/url-checker/pkg/sshTunnel"
)

/*
clear database: update test.`entities` set status="new", errormessage=null,mimetype=null,lastcheck=null,lastchange=null WHERE sig <> "xxx"
*/

func main() {
	configFile := flag.String("cfg", "./memocrawler.toml", "config file location")
	flag.Parse()

	var exPath = ""
	// if configfile not found try path of executable as prefix
	if !memostream.FileExists(*configFile) {
		ex, err := os.Executable()
		if err != nil {
			panic(err)
		}
		exPath = filepath.Dir(ex)
		if memostream.FileExists(filepath.Join(exPath, *configFile)) {
			*configFile = filepath.Join(exPath, *configFile)
		} else {
			log.Fatalf("cannot find configuration file: %v", *configFile)
			return
		}
	}
	// configfile should exists at this place
	var config Config
	config = LoadConfig(*configFile)

	// create logger instance
	log, lf := memostream.CreateLogger("urlchecker", config.Logfile, config.Loglevel)
	defer lf.Close()

	if config.SSHTunnel.User != "" && config.SSHTunnel.PrivateKey != "" {
		tunnels := map[string]*sshtunnel.SourceDestination{}
		tunnels["postgres"] = &sshtunnel.SourceDestination{
			Local: &sshtunnel.Endpoint{
				Host: config.SSHTunnel.LocalEndpoint.Host,
				Port: config.SSHTunnel.LocalEndpoint.Port,
			},
			Remote: &sshtunnel.Endpoint{
				Host: config.SSHTunnel.RemoteEndpoint.Host,
				Port: config.SSHTunnel.RemoteEndpoint.Port,
			},
		}
		tunnel, err := sshtunnel.NewSSHTunnel(
			config.SSHTunnel.User,
			config.SSHTunnel.PrivateKey,
			&sshtunnel.Endpoint{
				Host: config.SSHTunnel.ServerEndpoint.Host,
				Port: config.SSHTunnel.ServerEndpoint.Port,
			},
			tunnels,
			log,
		)
		if err != nil {
			log.Errorf("cannot create sshtunnel %v@%v:%v - %v", config.SSHTunnel.User, config.SSHTunnel.ServerEndpoint.Host, &config.SSHTunnel.ServerEndpoint.Port, err)
			return
		}
		if err := tunnel.Start(); err != nil {
			log.Errorf("cannot create sshtunnel %v - %v", tunnel.String(), err)
			return
		}
		defer tunnel.Close()
		time.Sleep(2 * time.Second)
	}
	// declare parameter ...?tls=custom in dsn to use certificate
	if strings.Contains(config.DB.Dsn, "tls=custom") {
		rootCertPool := x509.NewCertPool()
		pem, err := os.ReadFile("/etc/ssl/certs/ca-certificates.crt")
		if err != nil {
			log.Panicf("error reading ca certificates: %v", err)
			return
		}
		if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
			log.Panicf("can't append certificates to store")
			return
		}
		mysql.RegisterTLSConfig("custom", &tls.Config{RootCAs: rootCertPool})
	}

	db, err := sql.Open("mysql", config.DB.Dsn)
	if err != nil {
		// don't write dsn in error message due to password inside
		log.Panicf("error connecting to database: %v", err)
		return
	}
	defer db.Close()
	if err := db.Ping(); err != nil {
		log.Panicf("cannot ping database: %v", err)
		return
	}
	db.SetConnMaxLifetime(time.Duration(config.DB.ConnMaxTimeout.Duration))

	if err != nil {
		// don't write dsn in error message due to password inside
		log.Panicf("cannot initialize DBMySQL: %v", err)
		return
	}
	defer db.Close()

	mapping := map[string]string{}
	for _, val := range config.FileMap {
		mapping[strings.ToLower(val.Alias)] = val.Folder
	}
	fm := memostream.NewFilesystemDisk(mapping)

	cr := memocrawler.NewCrawler(
		db,
		config.Crawler.Workers,
		config.Crawler.PageSize,
		config.DB.Schema,
		config.TempDir,
		config.InsecureCert,
		config.IgnoreHTTPError,
		config.IgnoreSignature,
		config.Crawler.OK.Duration,
		config.Crawler.Error.Duration,
		config.Crawler.ErrorNew.Duration,
		config.Metadata.Timeout.Duration,
		config.Metadata.Workers,
		config.Metadata.PageSize,
		config.Crawler.HeaderSize,
		config.WebTimeout.Duration,
		config.Indexer,
		fm,
		log)

	if err := cr.Start(); err != nil {
		log.Errorf("error crawling: %v", err)
	}

}
