package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
)

type duration struct {
	time.Duration
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

type CfgDBMySQL struct {
	Dsn            string
	ConnMaxTimeout duration
	Schema         string
	Tls            bool
}

type FileMap struct {
	Alias  string
	Folder string
}

type Meta struct {
	Timeout  duration
	Workers  int
	PageSize int
	Cron     string
}

type Crawler struct {
	OK         duration
	Error      duration
	ErrorNew   duration
	Cron       string
	Timeout    duration
	PageSize   int
	HeaderSize int
	Workers    int
}

type Endpoint struct {
	Host string `toml:"host"`
	Port int    `toml:"port"`
}

type SSHTunnel struct {
	User           string   `toml:"user"`
	PrivateKey     string   `toml:"privatekey"`
	LocalEndpoint  Endpoint `toml:"localendpoint"`
	ServerEndpoint Endpoint `toml:"serverendpoint"`
	RemoteEndpoint Endpoint `toml:"remoteendpoint"`
}

type Config struct {
	Logfile         string
	Loglevel        string
	CertPEM         string
	KeyPEM          string
	InsecureCert    bool
	IgnoreHTTPError []int
	IgnoreSignature []string
	Addr            string
	JwtKey          string
	JwtAlg          []string
	SSHTunnel       SSHTunnel `toml:"sshtunnel"`
	DB              CfgDBMySQL
	TempDir         string
	Indexer         string
	Crawler         Crawler
	Metadata        Meta
	FileMap         []FileMap
	WebTimeout      duration
}

func LoadConfig(filepath string) Config {
	var conf Config
	_, err := toml.DecodeFile(filepath, &conf)
	if err != nil {
		log.Fatalln("Error on loading config: ", err)
	}
	dsn, exists := os.LookupEnv("DSN")
	if exists {
		conf.DB.Dsn = dsn
	} else {
		mariadb_user, exists := os.LookupEnv("MARIADB_USER")
		if !exists {
			log.Fatalln("No Mariadb user defined!")
		}
		mariadb_password, exists := os.LookupEnv("MARIADB_PASSWORD")
		if !exists {
			log.Fatalln("No Mariadb password defined!")
		}
		mariadb_host, exists := os.LookupEnv("MARIADB_HOST")
		if !exists {
			log.Fatalln("No Mariadb host defined!")
		}
		mariadb_port, exists := os.LookupEnv("MARIADB_PORT")
		if !exists {
			log.Fatalln("No Mariadb port defined!")
		}
		mariadb_database, exists := os.LookupEnv("MARIADB_DATABASE")
		if !exists {
			log.Fatalln("No Mariadb database defined!")
		}
		if conf.DB.Tls {
			conf.DB.Dsn = fmt.Sprintf(
				"%s:%s@tcp(%s:%s)/%s?tls=custom",
				strings.Trim(mariadb_user, "\n"),
				strings.Trim(mariadb_password, "\n"),
				mariadb_host,
				mariadb_port,
				mariadb_database,
			)
		} else {
			conf.DB.Dsn = fmt.Sprintf(
				"%s:%s@tcp(%s:%s)/%s",
				strings.Trim(mariadb_user, "\n"),
				strings.Trim(mariadb_password, "\n"),
				mariadb_host,
				mariadb_port,
				mariadb_database,
			)
		}
	}
	if conf.WebTimeout.Duration == 0 {
		conf.WebTimeout.Duration = 5 * time.Second
	}
	return conf
}
