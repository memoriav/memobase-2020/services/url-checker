FROM golang:1.23 as builder
RUN adduser --system appuser

WORKDIR $GOPATH/src/gitlab.switch.ch/memoriav/memobase-2020/services/url-checker
COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o bin/app -a gitlab.switch.ch/memoriav/memobase-2020/services/url-checker/cmd/main

FROM scratch
WORKDIR /app
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/src/gitlab.switch.ch/memoriav/memobase-2020/services/url-checker/bin/app /app
COPY --from=builder /etc/passwd /etc/passwd

USER appuser
EXPOSE 81

ENTRYPOINT ["/app/app"]
