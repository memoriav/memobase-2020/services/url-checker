// This file is part of Memobase Crawler which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memocrawler

import (
	"container/list"
	"errors"
	"fmt"
	"github.com/op/go-logging"
	"gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/memostream"
	"sync"
	"time"
)

// Represents the data needed to stream media object
type MediaEntry struct {
	Signature               string
	URI                     string
	Protocol                memostream.MediaProtocol
	Access                  memostream.MediaAccess
	Status                  memostream.MediaStatus
	Type                    string
	Mimetype                string
	Width, Height, Duration int64
	ManifestV2, ManifestV3  string
}

func NewMediaEntry(signature, uri, access, protocol, status, t, mimetype string, width, height, length int64, manifestV2, manifestV3 string) (*MediaEntry, error) {
	p, ok := memostream.MediaProtocolString[protocol]
	// invalid data in database
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid protocol value %s", protocol))
	}

	a, ok := memostream.MediaAccessString[access]
	// invalid data in database
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid access value %s", access))
	}

	s, ok := memostream.MediaStatusString[status]
	// invalid data in database
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid statuss value %s", status))
	}

	return &MediaEntry{
		Signature:  signature,
		URI:        uri,
		Protocol:   p,
		Access:     a,
		Status:     s,
		Type:       t,
		Mimetype:   mimetype,
		Width:      width,
		Height:     height,
		Duration:   length,
		ManifestV2: manifestV2,
		ManifestV3: manifestV3,
	}, nil
}

type Job struct {
	ID    string
	cr    *Crawler
	entry *MediaEntry
}

func (j *Job) GetID() string {
	return j.ID
}

type JobQueue struct {
	sync.Mutex
	queue    chan *Job
	waiting  *list.List
	wcounter int
	log      *logging.Logger
	doStop   bool
}

func NewJobQueue(queue chan *Job, log *logging.Logger) *JobQueue {
	return &JobQueue{queue: queue, waiting: list.New(), log: log, doStop: false}
}

func (jq *JobQueue) GetChannel() chan *Job {
	return jq.queue
}

func (jq *JobQueue) addWorkerCounter(num int) {
	jq.Lock()
	defer jq.Unlock()

	jq.wcounter += num
}
func (jq *JobQueue) isIdle() bool {
	jq.Lock()
	defer jq.Unlock()
	return jq.wcounter == 0 && jq.waiting.Len() == 0
}

func (jq *JobQueue) findWaiting(job *Job) *list.Element {
	for e := jq.waiting.Front(); e != nil; e = e.Next() {
		if (e.Value.(*Job)).ID == job.ID {
			return e
		}
	}
	return nil
}

func (jq *JobQueue) AddBack(job *Job) error {
	jq.Lock()
	defer jq.Unlock()

	jq.waiting.PushBack(job)

	return nil
}

func (jq *JobQueue) AddFront(job *Job) error {
	jq.Lock()
	defer jq.Unlock()

	jq.waiting.PushFront(job)

	return nil
}

/*
fills up job queue whenever it is possible
*/
func (jq *JobQueue) Start() {
	jq.log.Info("starting jobQueue")

	go func() {
		for {
			// stop endless loop
			if jq.doStop {
				jq.log.Info("received stop command - closing command queue")

				jq.Lock()
				defer jq.Unlock()
				return
			}
			// take the first element
			e := jq.waiting.Front()
			// if it exists...
			if e != nil {
				// ...and send it to the job queue
				jq.queue <- e.Value.(*Job)
				jq.Lock()
				// ...remove it...
				jq.waiting.Remove(e)
				jq.Unlock()
			} else {
				time.Sleep(3 * time.Second)
			}
		}
	}()
}

func (jq *JobQueue) Stop() {
	jq.log.Info("send signal for stop")
	jq.Lock()
	defer jq.Unlock()
	jq.doStop = true
}
