// This file is part of Memobase Crawler which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memocrawler

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"github.com/goph/emperror"
	"gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/memostream"
	"io"
	"net/http"
	"net/url"
	"strings"
)

type Worker struct {
	id       int
	cr       *Crawler
	jobQueue *JobQueue
}

//var w.cr.headerSize int64 = 512
//var timeout time.Duration = 5 * time.Second

// NewWorker creates takes a numeric id and a channel w/ worker pool.
func NewWorker(id int, cr *Crawler, jobQueue *JobQueue) Worker {
	return Worker{
		id:       id,
		jobQueue: jobQueue,
		cr:       cr,
	}
}

func (w Worker) checkFile(entry *MediaEntry) (string, error) {
	// Create url and check uri syntax
	url, err := url.Parse(entry.URI)
	if err != nil {
		return "", emperror.Wrapf(err, "cannot parse uri %s", entry.URI)
	}
	if url == nil {
		return "", emperror.Wrapf(err, "url from uri is nil %s", entry.URI)
	}

	rsc, _, err := w.cr.mapping.Open(*url)
	if err != nil {
		return "", emperror.Wrapf(err, "cannot open file")
	}
	defer rsc.Close()
	buf := make([]byte, 0, w.cr.headerSize)
	bbuf := bytes.NewBuffer(buf)
	written, err := io.CopyN(bbuf, rsc, int64(w.cr.headerSize))
	if err != nil {
		return "", emperror.Wrapf(err, "cannot read from %s", url.String())
	}
	if written != int64(w.cr.headerSize) {
		return "", fmt.Errorf("could not read %v bytes from %s - %v bytes read", w.cr.headerSize, url.String(), written)
	}
	return "", nil
}

func (w Worker) checkURL(entry *MediaEntry) (string, error) {
	// Create url and check uri syntax
	url, err := url.Parse(entry.URI)
	if err != nil {
		return "", emperror.Wrapf(err, "cannot parse uri %s", entry.URI)
	}
	if url == nil {
		return "", emperror.Wrapf(err, "url from uri is nil %s", entry.URI)
	}

	customTransport, ok := http.DefaultTransport.(*http.Transport)
	if !ok {
		return "", fmt.Errorf("http.DefaultTransport no (*http.Transport)")
	}
	customTransport = customTransport.Clone()
	customTransport.TLSClientConfig = &tls.Config{InsecureSkipVerify: w.cr.insecureCert}
	client := &http.Client{Transport: customTransport}
	ctx, _ := context.WithTimeout(context.Background(), w.cr.webTimeout)
	req, err := http.NewRequestWithContext(ctx, "GET", url.String(), nil)
	if err != nil {
		return "", emperror.Wrapf(err, "cannot create GET request for %s", url.String())
	}
	// we'll try a range request
	req.Header.Set("Range", fmt.Sprintf("bytes=0-%v", w.cr.headerSize-1))
	resp, err := client.Do(req)
	if err != nil {
		return "", emperror.Wrapf(err, "cannot execute GET request for %s", url.String())
	}
	defer resp.Body.Close()
	msg := fmt.Sprintf("response status: %s", resp.Status)
	// too many requests
	for _, code := range w.cr.ignoreHTTTPError {
		if resp.StatusCode == code {
			return msg, nil
		}
	}
	if resp.StatusCode >= 400 {
		return "", fmt.Errorf("response status %s", resp.Status)
	}
	buf := make([]byte, 0, w.cr.headerSize)
	bbuf := bytes.NewBuffer(buf)
	written, err := io.CopyN(bbuf, resp.Body, int64(w.cr.headerSize))
	if err != nil {
		return "", emperror.Wrapf(err, "cannot read from %s", url.String())
	}
	if written != int64(w.cr.headerSize) {
		return "", fmt.Errorf("could not read %v bytes from %s - %v bytes read", w.cr.headerSize, url.String(), written)
	}
	return "", nil
}

func (w Worker) start() {
	w.cr.log.Infof("#%03d starting Worker ", w.id)
	go func() {
		for job := range w.jobQueue.GetChannel() {
			w.jobQueue.addWorkerCounter(1)
			w.cr.log.Debugf("#%03d [%v] starting job", w.id, job.GetID())
			w.Do(job)
			w.cr.log.Debugf("#%03d [%v] completed job", w.id, job.GetID())
			w.jobQueue.addWorkerCounter(-1)
		}
	}()
}

func (w Worker) stop() {
	//close(w.jobInput)
}

func (w Worker) Do(job *Job) (err error) {
	w.cr.log.Debugf("#%03d [%s] checking %s - %s", w.id, job.entry.Signature, memostream.MediaProtocolNum[job.entry.Protocol], job.entry.URI)
	var msg string

	// check whether to ignore signatures
	ignore := false
	for _, prefix := range w.cr.ignoreSignature {
		if strings.HasPrefix(job.entry.Signature, prefix) {
			ignore = true
			break
		}
	}
	if ignore {
		w.cr.log.Infof("ignoring signature %s", job.entry.Signature)
		msg = "ignored signature"
	} else {
		switch job.entry.Protocol {
		case memostream.Media_File:
			msg, err = w.checkFile(job.entry)
		default:
			msg, err = w.checkURL(job.entry)
		}
	}
	var sqlstr string
	var params []interface{}
	if err != nil {
		errstr := err.Error()
		w.cr.log.Debugf("#%03d error checking url: %s", w.id, errstr)
		if job.entry.Status == memostream.Media_Error {
			sqlstr = fmt.Sprintf("UPDATE %s.entities SET errormessage=?, lastcheck=NOW() WHERE sig=?", w.cr.schema)
			params = []interface{}{
				errstr,
				job.entry.Signature,
			}
		} else {
			sqlstr = fmt.Sprintf("UPDATE %s.entities SET status=?, errormessage=?, lastcheck=NOW(), lastchange=NOW() WHERE sig=?", w.cr.schema)
			params = []interface{}{
				memostream.MediaStatusNum[memostream.Media_Error],
				errstr,
				job.entry.Signature,
			}
		}
	} else {
		w.cr.log.Debugf("#%03d [%s] ok", w.id, job.entry.Signature)
		if job.entry.Status == memostream.Media_OK {
			sqlstr = fmt.Sprintf("UPDATE %s.entities SET errormessage=?, lastcheck=NOW() WHERE sig=?", w.cr.schema)
			params = []interface{}{
				msg,
				job.entry.Signature,
			}
		} else {
			sqlstr = fmt.Sprintf("UPDATE %s.entities SET status=?, errormessage=?, lastcheck=NOW(), lastchange=NOW() WHERE sig=?", w.cr.schema)
			params = []interface{}{
				memostream.MediaStatusNum[memostream.Media_OK],
				msg,
				job.entry.Signature,
			}
		}
	}
	w.cr.log.Debugf("#%v [%s] updating database '%s' - %v", w.id, job.entry.Signature, sqlstr, params)
	defer w.cr.log.Debugf("#%v [%s] updating database done", w.id, job.entry.Signature)
	if _, err := w.cr.db.Exec(sqlstr, params...); err != nil {
		return emperror.Wrapf(err, "cannot update database - %s - %v", sqlstr, params)
	}
	return err
}
