// This file is part of Memobase Crawler which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memocrawler

import (
	"database/sql"
	"fmt"
	"github.com/goph/emperror"
	"github.com/op/go-logging"
	"gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/memostream"
	"time"
)

type Crawler struct {
	db               *sql.DB
	log              *logging.Logger
	workers          int
	pageSize         int
	schema           string
	tempDir          string
	crawlOK          time.Duration
	crawlError       time.Duration
	crawlErrorNew    time.Duration
	metaTimeout      time.Duration
	metaWorkers      int
	metaPageSize     int
	indexer          string
	bannerfolder     string
	bannertimeout    time.Duration
	mapping          memostream.Filesystem
	jobQueue         *JobQueue
	jobChannel       chan *Job
	metaJobQueue     *JobQueue
	metaJobChannel   chan *Job
	bannerJobQueue   *JobQueue
	bannerJobChannel chan *Job
	headerSize       int
	webTimeout       time.Duration
	insecureCert     bool
	ignoreHTTTPError []int
	ignoreSignature  []string
}

func NewCrawler(
	db *sql.DB,
	workers, pageSize int,
	schema, tempDir string,
	insecureCert bool,
	ignoreHttpError []int,
	ignoreSignature []string,
	crawlOK, crawlError, crawlErrorNew, metaTimeout time.Duration,
	metaWorkers, metaPageSize, headerSize int,
	webTimeout time.Duration,
	indexer string,
	mapping memostream.Filesystem,
	log *logging.Logger) *Crawler {
	cr := &Crawler{
		db:               db,
		workers:          workers,
		pageSize:         pageSize,
		schema:           schema,
		tempDir:          tempDir,
		indexer:          indexer,
		insecureCert:     insecureCert,
		ignoreHTTTPError: ignoreHttpError,
		ignoreSignature:  ignoreSignature,
		crawlOK:          crawlOK,
		crawlError:       crawlError,
		crawlErrorNew:    crawlErrorNew,
		metaTimeout:      metaTimeout,
		metaWorkers:      metaWorkers,
		metaPageSize:     metaPageSize,
		headerSize:       headerSize,
		webTimeout:       webTimeout,
		mapping:          mapping,
		log:              log,
	}
	return cr
}

/*
load all entries from query in array
*/
func (cr *Crawler) getEntries(sqlstr string, args ...interface{}) ([]*MediaEntry, error) {
	entries := []*MediaEntry{}

	sqlstr += " LIMIT 0, ?"
	args = append(args, cr.pageSize)

	rows, err := cr.db.Query(sqlstr, args...)
	if err == sql.ErrNoRows { // dataset not found
		return entries, nil
	}
	if err != nil { // something strange happenz
		return nil, emperror.Wrapf(err, "error querying %s", sqlstr)
	}
	defer rows.Close()
	for rows.Next() {
		var signature, access, protocol, uri, status string
		// get data
		if err := rows.Scan(&signature, &uri, &access, &protocol, &status); err != nil {
			return nil, emperror.Wrapf(err, "cannot scan values")
		}
		me, err := NewMediaEntry(signature, uri, access, protocol, status, "", "", 0, 0, 0, "", "")
		if err != nil {
			return nil, emperror.Wrapf(err, "cannot create MediaEntry")
		}
		entries = append(entries, me)
	}
	return entries, nil
}

func (cr *Crawler) CrawlNew() error {
	cr.log.Infof("start crawling new entities")

	sqlstr := fmt.Sprintf("SELECT sig AS signature, uri, access, proto AS protocol, `status` "+
		"FROM %s.entities "+
		"WHERE status='new' "+
		"ORDER BY creationtime ASC", cr.schema)
	for {
		entries, err := cr.getEntries(sqlstr)
		if err != nil {
			return emperror.Wrapf(err, "cannot get new entries")
		}
		if len(entries) == 0 {
			break
		}
		for _, entry := range entries {
			cr.jobQueue.AddBack(&Job{
				ID:    entry.Signature,
				cr:    cr,
				entry: entry,
			})
		}

		/*
			if err := cr.checkList(entries); err != nil {
				return emperror.Wrapf(err, "cannot linkCheck result list")
			}
		*/
		// wait until last worker ist done
		for {
			if cr.jobQueue.isIdle() {
				break
			}
			time.Sleep(1 * time.Second)
		}
	}
	return nil
}

func (cr *Crawler) CrawlError() error {
	cr.log.Infof("start crawling entities with errors")

	sqlstr := fmt.Sprintf("SELECT sig AS signature, uri, access, proto AS protocol, `status` "+
		" FROM %s.entities"+
		" WHERE status='error'"+
		" AND ("+
		"	lastcheck < lastchange"+
		" 	OR ("+
		"		lastcheck > TIMESTAMPADD(SECOND, ?, NOW())"+
		" 		AND lastcheck < TIMESTAMPADD(SECOND, ?, NOW())"+
		"	)"+
		")"+
		" ORDER BY lastchange ASC", cr.schema)
	for {
		entries, err := cr.getEntries(sqlstr, int64(cr.crawlErrorNew/time.Second), int64(cr.crawlError/time.Second))
		if err != nil {
			return emperror.Wrapf(err, "cannot get new entries")
		}
		if len(entries) == 0 {
			break
		}

		for _, entry := range entries {
			cr.jobQueue.AddBack(&Job{
				ID:    entry.Signature,
				cr:    cr,
				entry: entry,
			})
		}

		// wait until last worker ist done
		for {
			if cr.jobQueue.isIdle() {
				break
			}
			time.Sleep(1 * time.Second)
		}
	}
	return nil
}

func (cr *Crawler) CrawlOK() error {
	cr.log.Infof("start crawling entities without errors")

	var lastFirst *MediaEntry
	sqlstr := fmt.Sprintf("SELECT sig AS signature, uri, access, proto AS protocol, `status` "+
		"FROM %s.entities "+
		"WHERE status='ok' "+
		"AND lastcheck >= TIMESTAMPADD(SECOND, ?, NOW())"+
		"ORDER BY lastcheck ASC", cr.schema)
	for {
		entries, err := cr.getEntries(sqlstr, int64(cr.crawlOK/time.Second))
		if err != nil {
			return emperror.Wrapf(err, "cannot get new entries")
		}
		if len(entries) == 0 {
			break
		}
		first := entries[0]
		if lastFirst != nil {
			if first.Signature == lastFirst.Signature {
				return fmt.Errorf("runthrough has no effect for signature %s", lastFirst.Signature)
			}
		}
		lastFirst = first

		for _, entry := range entries {
			cr.jobQueue.AddBack(&Job{
				ID:    entry.Signature,
				cr:    cr,
				entry: entry,
			})
		}

		// wait until last worker ist done
		for {
			if cr.jobQueue.isIdle() {
				break
			}
			time.Sleep(1 * time.Second)
		}
	}
	return nil
}

func (cr *Crawler) Start() error {
	// setup jobs
	cr.jobChannel = make(chan *Job)
	cr.jobQueue = NewJobQueue(cr.jobChannel, cr.log)
	cr.jobQueue.Start()

	for i := 0; i < cr.workers; i++ {
		w := NewWorker(i+1, cr, cr.jobQueue)
		w.start()
	}

	cr.log.Infof("start crawling")
	if err := cr.CrawlNew(); err != nil {
		return emperror.Wrap(err, "error crawling new entities")
	}
	if err := cr.CrawlError(); err != nil {
		return emperror.Wrap(err, "error crawling error entities")
	}
	if err := cr.CrawlOK(); err != nil {
		return emperror.Wrap(err, "error crawling entities")
	}
	return nil
}
