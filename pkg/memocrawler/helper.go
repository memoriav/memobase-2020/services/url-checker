// This file is part of Memobase Crawler which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memocrawler

import (
	"github.com/op/go-logging"
	"strings"
)

var _logformat = logging.MustStringFormatter(
	`%{time:2006-01-02T15:04:05.000} %{module}::%{shortfunc} [%{shortfile}] > %{level:.5s} - %{message}`,
)

/*
holistic function to give some mimetypes a relevance
 */
func MimeRelevance( mimetype string) (relevance int) {
	if mimetype == "" {
		return 0
	}
	if mimetype == "application/octet-stream" {
		return 1
	}
	if mimetype == "text/plain" {
		return 2
	}
	if strings.HasPrefix(mimetype, "application/") {
		return 3
	}
	if strings.HasPrefix(mimetype, "text/") {
		return 4
	}
	return 1000
}